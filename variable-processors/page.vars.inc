<?php

function terrain_preprocess_page(&$vars) {
 
  // Default Layout
  $page_attr           = &$vars['html_elements']['page']['#attributes'];
  $page_content_wrapper_attr  = &$vars['html_elements']['page_content_wrapper']['#attributes'];
  $content_header_attr = &$vars['html_elements']['content_header']['#attributes'];
  $content_upper_attr  = &$vars['html_elements']['content_upper']['#attributes'];
  $content_attr        = &$vars['html_elements']['content']['#attributes'];
  $sidebar_accordion_attr        = &$vars['html_elements']['sidebar_accordion']['#attributes'];
  $sidebar_attr        = &$vars['html_elements']['sidebar']['#attributes'];
  $skinny_attr         = &$vars['html_elements']['skinny']['#attributes'];
  $callout_attr   = &$vars['html_elements']['callout']['#attributes'];
  $footer_wrapper_attr = &$vars['html_elements']['footer_wrapper']['#attributes'];
  $footer_inner_attr   = &$vars['html_elements']['footer_inner']['#attributes'];
  $footer_content_attr = &$vars['html_elements']['footer_content']['#attributes'];
  $footer_message_attr = &$vars['html_elements']['footer_message']['#attributes'];
  $footer_right_attr   = &$vars['html_elements']['footer_right']['#attributes'];

  // Common to all layouts
  $page_attr['class'][] = 'container-6';
  $page_content_wrapper_attr['class'][] = 'content-container-6' .' '. 'clearfix';
  $footer_wrapper_attr['class'][] = 'content-container-6';
  $footer_inner_attr['class'][] = 'grid-5';
  $footer_content_attr['class'][] = 'grid-5' .' '. 'alpha' .' '. 'omega';
  $footer_message_attr['class'][] = 'grid-5' .' '. 'alpha' .' '. 'omega';
  $footer_right_attr['class'][] = 'grid-1';

  // Default Layout
  $content_default = 'grid-4';
  $sidebar_default = 'grid-2';
  $sidebar_accordion_default = 'grid-2';

  // Full Width Layout
  $content_full  = 'grid-6';

  // Content Upper Layout
  $content_upper = 'grid-6';

  // Sidebar Left Layout
  $content_left = 'grid-4' .' '. 'push-2';
  $sidebar_left = 'grid-2' .' '. 'pull-4';
  $callout_left = 'layout-left';

  // Skinny Layouts Content Header
  $content_header_skinny = 'grid-6';

  // Skinny Left Layout
  $content_skinnyl = 'grid-3' .' '. 'push-1';
  $skinny_skinnyl = 'grid-1' .' '. 'pull-5';
  $sidebar_skinnyl = 'grid-2' .' '. 'push-1';

  // Skinny Right Layout
  $content_skinnyr = 'grid-3' .' '. 'push-2';
  $sidebar_skinnyr = 'grid-2' .' '. 'pull-3';
  $skinny_skinnyr = 'grid-1';

  // Check for context layout module
  $context_layout = '';
  if (function_exists('context_layouts_get_active_layout')) {
    // Get the active context layout
    $context_layout = context_layouts_get_active_layout(FALSE);
  }

  switch ($context_layout) {
    case 'full':
      $content_attr['class'][] = $content_full;
      break;
    case 'upper':
      $content_upper_attr['class'][] = $content_upper;
      $content_attr['class'][] = $content_default;
      $sidebar_attr['class'][] = $sidebar_default;
      $sidebar_accordion_attr['class'][] = $sidebar_accordion_default;
      break;
    case 'left':
      $content_attr['class'][] = $content_left;
      $sidebar_attr['class'][] = $sidebar_left;
      $callout_attr['class'][] = $callout_left;
      break;
    case 'skinnyl':
      $content_header_attr['class'][] = $content_header_skinny;
      $content_attr['class'][] = $content_skinnyl;
      $skinny_attr['class'][] = $skinny_skinnyl;
      $sidebar_attr['class'][] = $sidebar_skinnyl;
      break;
    case 'skinnyr':
      $content_header_attr['class'][] = $content_header_skinny;
      $content_attr['class'][] = $content_skinnyr;
      $skinny_attr['class'][] = $skinny_skinnyr;
      $sidebar_attr['class'][] = $sidebar_skinnyr;
      break;
    default:
      $content_attr['class'][] = $content_default;
      $sidebar_attr['class'][] = $sidebar_default;
      $sidebar_accordion_attr['class'][] = $sidebar_accordion_default;
    }

}
