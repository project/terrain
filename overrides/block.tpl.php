<?php
// $Id: block.tpl.php,v 1.1.2.1 2010/05/14 23:47:42 dvessel Exp $
?>
<div<?php print_select($html_elements, '#attributes') ?>>
  <?php if ($block->subject): ?><h2 class="block-title title"><?php print $block->subject ?></h2><?php endif?>
  <div class="block-content content"><?php print $block->content ?></div>
</div>
