<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head ?>
  <title><?php print $head_title ?></title>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body<?php print_select($html_elements, '#attributes') ?>>
  <div id="page" class="clearfix <?php print_select($html_elements, 'page#attributes.class') ?>">

    <?php if ($logo_img || $site_name || $site_slogan): ?>
    <div id="site-branding"<?php print_select($html_elements, 'site_branding#attributes.class+') ?>>
      <?php if ($logo_img || $site_name): ?>
      <h2 id="site-id"><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>" rel="home">
        <?php print $logo_img // img.logo ?>
        <?php if ($site_name): ?><span class="site-name"><?php print $site_name ?></span><?php endif ?>
      </a></h2>
      <?php endif ?>
      <?php if ($site_slogan): ?><p class="site-slogan"><?php print $site_slogan ?></p><?php endif ?>
    </div>
    <?php endif ?>


    <?php if ($primary_links || $secondary_links || $search_box): ?>
    <div id="site-navigation"<?php print_select($html_elements, 'site_navigation#attributes.class+') ?>>
      <?php print theme('links', $primary_links, array('class' => 'navigation primary links'))
        // ul.navigation.primary.links > li.menu-ID[.first|.last] > a[.active] ?>
      <?php print theme('links', $secondary_links, array('class' => 'navigation secondary links'))
        // ul.navigation.secondary.links > li.menu-ID[.first|.last] > a[.active] ?>
      <?php print $search_box
        // form#search-theme-form > div > ... ?>
    </div>
    <?php endif ?>


    <?php if ($messages || $header || $mission): ?>
    <div id="page-header"<?php print_select($html_elements, 'page_header#attributes.class+') ?>>
      <?php print $messages
        // div.status-messages > ul.messages[.status|.error] > li.message ?>
      <?php print $header
        // div#blocks-header.blocks > div#block-ID.block.block-MODULE ?>
      <?php if ($mission): ?><div id="site-mission"><?php print $mission ?></div><?php endif ?>
    </div>
    <?php endif ?>


    <div id="page-content-wrapper">
      <?php if ($title || $tabs || $help || $breadcrumb): ?>
      <div id="content-header"<?php print_select($html_elements, 'content_header#attributes.class+') ?>>
        <?php print $breadcrumb ?>
        <?php if ($title): ?><h1 class="page-title title"><?php print $title ?></h1><?php endif ?>
        <?php print $tabs
          // ul.tabs[.primary|.secondary] > li > a[.active] ?>
        <?php print $help
          // div.help > p[.more-help-link] ?>
      </div>
      <?php endif ?>

      <div id="content"<?php print_select($html_elements, 'content#attributes.class+') ?>>
        <?php print $content
          // CONTENT
          // div#blocks-content.blocks > div#block-ID.block.block-MODULE ?>
        <?php print $feed_icons
          // a.feed-icon > img ?>
      </div>
    </div>


    <?php print $left
      // div#blocks-left.blocks > div#block-ID.block.block-MODULE ?>
    <?php print $right
      // div#blocks-right.blocks > div#block-ID.block.block-MODULE ?>


    <?php print $footer
      // div#blocks-footer.blocks > div#block-ID.block.block-MODULE ?>

    <?php if ($footer_message): ?>
    <div id="footer-message"<?php print_select($html_elements, 'footer_message#attributes.class+') ?>>
      <?php print $footer_message ?>
    </div>
    <?php endif ?>

  </div>
  <?php print $closure ?>
</body>
</html>
