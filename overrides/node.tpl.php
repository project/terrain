<?php
// $Id: node.tpl.php,v 1.1.2.1 2010/05/14 23:47:42 dvessel Exp $
?>
<div<?php print_select($html_elements, '#attributes') ?>>
<?php if (!$page): ?>
  <h2 class="node-title title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif ?>

<?php if ($submitted || $terms): ?>
  <div class="meta">
    <?php print $picture;
      // div.user-picture [a] img ?>
    <?php if ($submitted): ?><span class="submitted"><?php print $submitted ?></span><?php endif ?>
    <?php if ($terms): ?>
      <div class="terms">
        <?php print $terms;
          // ul.links.inline li.taxonomy_term_N[.first|.last] ?>
      </div>
    <?php endif?>
  </div>
<?php endif ?>

  <div class="node-content content">
    <?php print $content ?>
  </div>

  <?php print $links;
    // ul.links.inline li[.first|.last] ?>
</div>
