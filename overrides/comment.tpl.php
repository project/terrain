<?php
// $Id: comment.tpl.php,v 1.1.2.1 2010/05/14 23:47:42 dvessel Exp $
?>
<div<?php print_select($html_elements, '#attributes') ?>>
  <h3 class="comment-title title"><?php print $title; ?></h3>
  <div class="meta">
    <?php print $picture; ?>
    <?php print $submitted; ?>
    <?php if ($comment->new) : ?><span class="new"><?php print $new; ?></span><?php endif; ?>
  </div>
  <div class="comment-content content clear-block"><?php print $content; ?></div>
  <?php if ($signature): ?><div class="signature"><?php print $signature ?></div><?php endif ?>
  <?php print $links; ?>
</div>