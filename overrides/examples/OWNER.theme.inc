<?php
// $Id: OWNER.theme.inc,v 1.1.2.1 2010/05/15 20:07:06 dvessel Exp $

/**
 * This file can be used to hold a collection of theme function and variable
 * preprocessors for theme functions and/or templates. The collection is
 * determined by the originator of the theming hooks.
 * 
 * For example, the following hooks are defined by the core book module.
 * - book_navigation
 * - book_export_html
 * - book_admin_table
 * - book_title_link
 * - book_all_books_block
 * - book_node_export_html
 * 
 * All the functions associated to the theming hooks listed above can live in
 * a single file named after the parent module. It would be "book.theme.inc"
 * in this case.
 * 
 * This can apply for any module. It applies to themes if the theme implements
 * hook_theme() to supply its own custom hooks.
 */
