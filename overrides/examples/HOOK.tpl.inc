<?php
// $Id: HOOK.tpl.inc,v 1.1.2.2 2010/06/08 11:53:58 dvessel Exp $
/**
 * Just an example of a template. Nothing new here. See the following files
 * for possible locations of preprocess functions.
 * 
 * @see HOOK.vars.inc
 * @see OWNER.theme.inc
 */
?>
<div class="example-template">

  <h2 class="title"><?php print $title ?></h2>

  <div class="content">
    <?php print $content ?>
  </div>

</div>
