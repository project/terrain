<?php
// $Id: HOOK.func.inc,v 1.1.2.1 2010/05/15 20:07:06 dvessel Exp $

/**
 * Example theme hook implemented as a function. The base name of this file
 * (HOOK.func.inc) and the HOOK in the name this function can be any theme
 * function.
 * 
 * When overriding an existing theming hook, make sure the required parameters
 * are set. This information can be found in the Drupal API documentation site
 * for core theming hooks. (http://api.drupal.org/api)
 * 
 * For contrib modules, the source code can be referred to.
 * 
 * @see http://drupal.org/node/173880
 *  More information on theme overrides.
 * 
 * @see hex_starter_func_preprocess_HOOK()
 *  Preprocess functions for theme functions.
 * 
 * @see OWNER.theme.inc
 *  Where this function can also reside.
 */
function hex_starter_HOOK($arguments) {
  
}
