<?php
// $Id: HOOK.vars.inc,v 1.1.2.2 2010/06/08 11:53:58 dvessel Exp $

/**
 * Example theme hook variable preprocessor connected to templates. The base
 * name of this file (HOOK.vars.inc) and the 'HOOK' in the name of this
 * function can be any theming hook implemented as a template.
 * 
 * @see OWNER.theme.inc
 *  Where it can also be placed.
 * 
 * @see http://drupal.org/node/223430
 *  More information on preprocess functions.
 */
function hex_starter_preprocess_HOOK(&$vars) {
  
}

/**
 * Example theme hook variable preprocessor connected to a theme function. The
 * base name of this file (HOOK.vars.inc) and the HOOK in the name of
 * this function can be any theme function.
 * 
 * The $vars variable will contain all parameters passed through the original
 * theme function keyed to their purpose. This information can be found in the
 * Drupal API documentation site for core theming hooks. (http://api.drupal.org/api)
 * 
 * For contrib modules, the source code can be referred to.
 * 
 * In Drupal Core, preprocess functions are only used for themed templates, not
 * theme functions. Hexagon contains a modification to allow preprocessors
 * for theme functions but there are some differences.
 * 
 * - Generalized preprocessors are not allowed. Only hook specific preprocessors
 *   are permitted. This helps maintain the performance advantage for theme
 *   functions.
 * - Theme function preprocessors must be named with 'func' in
 *   'THEME_NAME_func_preprocess_HOOK' not 'THEME_NAME_preprocess_HOOK' which
 *   are used for preprocessors for templates. Since the nature of the code in
 *   the two types of implementations can differ drastically, this distinction
 *   is made to prevent unintentional errors when switching between types.
 * 
 * Note: Drupal 7 will have theme function preprocessors in core.
 * 
 * @see OWNER.theme.inc
 *  Where it can also be placed.
 */
function hex_starter_func_preprocess_HOOK(&$vars) {
  
}
